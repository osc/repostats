#!/usr/bin/env python
import subprocess as sp
from os import chdir, listdir
from datetime import datetime, timezone
import json
import gitlab
from collections import defaultdict
from shutil import rmtree
from gitlab.exceptions import GitlabListError

GIT_SERVER = "https://git.astron.nl"
WORK_DIR = "count_work_dir"
chdir(WORK_DIR)


def main():
    gl = gitlab.Gitlab(GIT_SERVER)
    language_stats = defaultdict(int)
    json_output_blob = {}
    for proj in gl.projects.list(iterator=True):
        repo_url = proj.http_url_to_repo
        try:
            project_commits = proj.commits.list(all=True)
        except GitlabListError:
            continue
        project_number_of_commits = len(project_commits)
        project_first_commit = project_commits[-1]
        project_first_commit_date = project_first_commit.attributes["created_at"]
        project_age = (
            datetime.now(timezone.utc) -
            datetime.fromisoformat(project_first_commit_date)
        ).days
        project_contributors = proj.repository_contributors(all=True)
        project_number_of_contributors = len(project_contributors)
        project_default_branch = proj.default_branch
        git_clone(repo_url)
        chdir(proj.path)
        notes, code_license, scc_data, total_size = analyse_repo()
        total_code, total_blank, total_comment = 0, 0, 0
        for data_language in scc_data:
            total_code += data_language["Code"]
            total_blank += data_language["Blank"]
            total_comment += data_language["Comment"]
            language_stats[data_language["Name"]] += data_language["Code"]
            language_stats["Blank"] += data_language["Blank"]
            language_stats["Comment"] += data_language["Comment"]
        json_output_blob[proj.http_url_to_repo] = {
            "contributor_count": project_number_of_contributors,
            "contributors" : project_contributors,
            "commits": project_number_of_commits,
            "age": project_age,
            "default branch": project_default_branch,
            "lines of code": total_code,
            "blank lines": total_blank,
            "lines of comments": total_comment,
            "code license": code_license
            }
        chdir("..")
        rmtree(proj.path)
    output_totals = {}
    for language in language_stats.keys():
        output_totals[language] = language_stats[language]
    json_output_blob["Languages"] = output_totals
    chdir("..")
    with open("gitlab_stats.json", "w") as statsfile:
        json.dump(json_output_blob, statsfile)


def git_clone(repo):
    command = ["git", "clone", repo]
    git_process = sp.Popen(command)
    git_process.communicate()


def analyse_repo():
    notes_present = "NOTES" in listdir() or "NOTES.md" in listdir()

    licensee_process = sp.Popen(["licensee", "--json"], stdout=sp.PIPE)
    licensee_data = json.loads(licensee_process.communicate()[0])

    try:
        license_key = licensee_data["licenses"][0]["key"]
    except IndexError:
        license_key = None

    scc_json_process = sp.Popen(["scc", "-f", "json"], stdout=sp.PIPE)
    scc_data_json = json.loads(scc_json_process.communicate()[0])

    scc_table_process = sp.Popen(["scc"], stdout=sp.PIPE)
    scc_data_table = scc_table_process.communicate()[0].decode().split("\n")
    totals_index = [idx for idx, val in enumerate(scc_data_table) if "Total" in val][0]
    scc_sizeline = scc_data_table[totals_index + 6]
    scc_total_size = int(scc_sizeline.split()[1])

    return notes_present, license_key, scc_data_json, scc_total_size


if __name__ == "__main__":
    main()
