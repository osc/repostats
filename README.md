# Repostats
The script in this repository will do several things to gather statistics from the gitlab API. For this you will need the `python-gitlab` package installed, which is listed in the `requirements.txt` file.

After gathering several pieces of information (number of commits, number of contributors, repository age in days) from the gitlab API, the script will go into a direcotry called `count_work_dir` and pull each repo, run [licensee](https://github.com/licensee/licensee) to determine what the license of the repo is and [scc](https://github.com/boyter/scc) to get an overview of lines of codes in different languages. To use the script you should make sure that those tools are installed and executable.

The output will be a JSON file called `gitlab_stats.json` with for each public repo:

```
  <repo URL>: {
    "contributors": <number of contributors>,
    "commits": <number of commits>,
    "age": <time since first commit in days,
    "default branch": <project default branch, mostly main or master>
    "lines of code": <lines of code>,
    "blank lines": <blank lines>,
    "lines of comments": <lines of comment>,
    "code license": "<identifier of the license>"
  }
```


  followed by an antry labeled `Languages` which lists for each language the total number of lines of code in that language. For example, it could look like this (which is actual data from a test run with 6 repos):

 ```
 "Languages": {
    "Plain Text": 1166,
    "Blank": 5470,
    "Comment": 6923,
    "C++": 10729,
    "C Header": 4089,
    "Python": 2922,
    "CMake": 1201,
    "ReStructuredText": 586,
    "Shell": 136,
    "Markdown": 418,
    "SVG": 3889,
    "Autoconf": 315,
    "YAML": 366,
    "C++ Header": 359,
    "Docker ignore": 8,
    "HTML": 14,
    "Jupyter": 1020,
    "License": 723,
    "MATLAB": 28,
    "TOML": 86,
    "gitignore": 6,
    "Dockerfile": 13,
    "INI": 68
  }
```


## Known issues
The number of commits is to be taken somewhat with a grain of salt. For some reason the total number of commits in some repos differs from the sum of the commits by each contributor. Also I think the default number of commits in the Gitlab API takes only the default branch into account. This is interesting but a bit too much investigation for the sake of this script. 
